

import { GridyViewDriver, ITEM_TPL_FULLPATH_AN } from "../../gridy-grid/src/view/gridy-view-driver.js";
import { ITEM_CSS_CN } from "../../gridy-grid/src/gridy-element.js";

export const ITEM_LOADING_IMAGE_FULLPATH_AN="item-loading-image-fullpath";
export const ITEM_PREVIEW_WIDTH_AN = 'item-preview-width';
export const ITEM_PREVIEW_WIDTH_DEFAULT = '200px';
export const ITEM_PREVIEW_HEIGHT_AN = 'item-preview-height';
export const ITEM_PREVIEW_HEIGHT_DEFAULT = '200px';

export const BLINK_CSS_CN = "icongrid-blinking";

export const ICONGRID_ITEM_SL = ".gridy-view-icongrid-item";

export class GridyViewIcongrid extends GridyViewDriver {
    
    
    get itemTplFullPath() {
        if (! this._itemTplFullPath) {
            this._itemTplFullPath = this.comp.hasAttribute(ITEM_TPL_FULLPATH_AN)
                ? this.getAttribute(ITEM_TPL_FULLPATH_AN) 
                : '/node_modules/gridy-view-icongrid/src/tpls/gridy-view-icongrid-item.tpl.html';
        }
        return this._itemTplFullPath;
    }
    
    get loadingImageFullPath() {
        if (! this._loadingImageFullPath) {
            this._loadingImageFullPath = this.comp.hasAttribute(ITEM_LOADING_IMAGE_FULLPATH_AN)
                ? this.getAttribute(ITEM_LOADING_IMAGE_FULLPATH_AN) 
                : '/node_modules/gridy-view-icongrid/src/tpls/loading-image.png';
        }
        return this._loadingImageFullPath;
    }
    
    get itemPreviewWidth() {
        return this.comp.hasAttribute(ITEM_PREVIEW_WIDTH_AN)
            ? this.comp.getAttribute(ITEM_PREVIEW_WIDTH_AN) : ITEM_PREVIEW_WIDTH_DEFAULT;
    }
        
    get itemPreviewHeight() {
        return this.comp.hasAttribute(ITEM_PREVIEW_HEIGHT_AN)
            ? this.comp.getAttribute(ITEM_PREVIEW_HEIGHT_AN) : ITEM_PREVIEW_HEIGHT_DEFAULT;
    }
    
    renderItems(target, fmtedData, tpl) {
        for (let item of fmtedData) {
            let html = '';
            if (tpl) {
                if (typeof tpl === 'string') {
                    let itemContentsEl = this.comp.renderer.createEl('div');
                    itemContentsEl.insertAdjacentHTML('beforeend', tpl);
                    html = this.comp.renderer.renderMustacheVars(itemContentsEl, item);
                } else {
                    let prepared = this.comp.renderer.prepareTemplate(tpl);
                    html = this.comp.renderer.renderMustacheVars(prepared, {...this.comp.tplVars, ...item});
                }
            } else {
                for (let fieldKey of Object.keys(this.fieldMappings)) {
                    html += `<span>${item[fieldKey]}</span>`;
                }
            }
            target.insertAdjacentHTML('beforeend', html);
            let items = target.querySelectorAll(ICONGRID_ITEM_SL);
            let curItem = items[items.length - 1];
            curItem.dataItem = item;
            let preview = curItem.querySelector(ICONGRID_ITEM_SL + '__preview');
            preview.classList.add(BLINK_CSS_CN);
            setTimeout(function() {
                fetch(item.url)
                .then(response => response.blob())
                .then(function(blob) {
                    let reader = new FileReader();                    
                    let imageUrl = URL.createObjectURL(blob);
                    preview.style.backgroundImage = `url(${imageUrl})`;
                    preview.classList.remove(BLINK_CSS_CN);
                });
            }, 0);

        }
    }
    
    async renderData(el, data) {
        this.comp.tplVars.itemPreviewWidth = this.itemPreviewWidth;
        this.comp.tplVars.itemPreviewHeight = this.itemPreviewHeight;
        this.comp.tplVars.loadingImageFullPath = this.loadingImageFullPath;
        
        await super.renderData(el, data);
        this.container.style.display = 'flex';
        this.container.style.flexWrap = 'wrap';
    }    
}
