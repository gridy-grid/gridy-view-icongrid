# Gridy Grid Icongrid View

grid with image previews loaded by urls


```html
<gridy-grid id="gridyGrid2" base-path="/node_modules/gridy-grid/src" sort-field="$.title">
    <gridy-data-source fields='[{ "title": "Name", "path": "$.name" },{ "title": "Url", "path": "$.url"}]'
                       datasource-type="DataSourceLocal" datapath="$.data"></gridy-data-source>
	<gridy-view
        dri="icongrid" id="gridyChart2" field-name="Name" field-url="Url"></gridy-view>
        
    <gridy-pager id="gridyPager2"></gridy-pager>
</gridy-grid>
```

### attributes

**item-preview-width** - maximum image width
**item-preview-height** - maximum image height
